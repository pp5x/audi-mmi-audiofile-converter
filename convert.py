#!/usr/bin/python
## convert.py for Audi MMI Audiofile Converter
##
## Made by Pierre Pagnoux
## <Pierre.Pagnoux@gmail.com>
##

from mutagen.mp4 import MP4
from mutagen.mp4 import MP4Cover

from mutagen.mp3 import MP3
from mutagen.id3 import ID3, APIC

from PIL import Image

from StringIO import StringIO
import sys
import os

SIZE = 500, 500

def covr_resize(buff):
	img = Image.open(buff)

	if img.size < SIZE:
		print "Cover size is too low."
		exit()

	img.thumbnail(SIZE, Image.ANTIALIAS)
	buff.seek(0)
	img.save(buff, 'JPEG')

def mp4_cover(filename):
	audiofile = MP4(filename)
	covrlist = audiofile.tags['covr']

	if not covrlist:
		return 1

	buff = StringIO(covrlist[0])

	covr_resize(buff)

	buff.seek(0)

	covr = []
	covr.append(MP4Cover(buff.getvalue(), MP4Cover.FORMAT_JPEG))

	audiofile.tags['covr'] = covr

	buff.close()
	audiofile.save()

	return 0

def mp3_cover(filename):

	audio = MP3(filename, ID3=ID3)

	if not 'APIC:' in audio.tags:
		return 1

	buff = StringIO(audio.tags['APIC:'].data)

	covr_resize(buff)

	buff.seek(0)

	audio.tags['APIC:'].data = buff.getvalue()
	audio.tags['APIC:'].mime = 'image/jpeg'

	buff.close()
	audio.save()

	return 0

def main():
	if len(sys.argv) == 1:
		print ("python <scriptname>.py audiofile.(mp3|m4a|mp4)\n"
			   "Warning : Modifications on audiofile can't be undone.")
		exit()

	filename = sys.argv[1]
	extention = os.path.splitext(filename)[1]

	if not os.path.exists(filename):
		print "Invalid file path."
		exit()

	if extention in ['.m4a','.mp4','.m4b','.m4p']:
		if mp4_cover(filename):
			print "No covers were found."
	elif extention == '.mp3':
		if mp3_cover(filename):
			print "No covers were found."
	else:
		print "Invalid audio file extention."

	exit()

if __name__ == '__main__':
	main()
